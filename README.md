# Simple Hiring System

Candidates apply for the position. HR enters and
updates records of the candidates. Hiring
managers prepare interview topics or a theme, and
evaluates candidates. HR notifies the candidates